mp = require 'multiprogress';
require 'paths';
require 'optim';
require 'nn';
require 'lua-code/LIFT_model';
dofile './provider.lua';

local c = require 'trepl.colorize'

opt = lapp[[
   -s,--save                  (default "desc_logs")      subdirectory to save logs
   -b,--batchSize             (default 128)          batch size
   -r,--learningRate          (default 1)        learning rate
   --learningRateDecay        (default 1e-7)      learning rate decay
   --weightDecay              (default 0.0005)      weightDecay
   -m,--momentum              (default 0.9)         momentum
   --epoch_step               (default 2)          epoch step
   --model                    (default lift_desc)     model name
   --max_epoch                (default 30)           maximum number of iterations
   --backend                  (default nn)            backend
   --type                     (default float)          cuda/float/cl
]]
print(opt)

local function cast(t)
	if opt.type == 'cuda' then 
		require 'cunn'
		return t:cuda()
	elseif opt.type == 'float' then 
		return t:float()
	else 
		error('Unknown type '..opt.type)
	end
end

print(c.blue '==>' ..' configuring model')
-- model configuration here !
local model = cast(model_siamese_desc('lua-code/LIFT_weight.mat'))

print(model)

print(c.blue '==>' ..' loading data')
-- data processing here ! 
provider = torch.load 'provider.t7'


print('Log will be saved at '..opt.save)
-- test log here ! 
paths.mkdir(opt.save)
paths.mkdir(paths.concat(opt.save, 'history_models'))
testLogger = optim.Logger(paths.concat(opt.save, 'test.log'))
testLogger:setNames{'% mean loss (train set)', '% mean loss (test set)'}
testLogger.showPlot = false

parameters,gradParameters = model:getParameters()


print(c.blue '==>' ..' setting criterion')
-- criterion configuration here ! 
margin = 1
criterion = cast(nn.HingeEmbeddingCriterion(margin))

print(c.blue'==>' ..' configuring optimizer')
optimState = {
  learningRate = opt.learningRate,
  weightDecay = opt.weightDecay,
  momentum = opt.momentum,
  learningRateDecay = opt.learningRateDecay,
}


function train()
	model:training()
	epoch = epoch or 1 

	-- drop learning rate every "epoch_step" epochs
  	if epoch % opt.epoch_step == 0 then optimState.learningRate = optimState.learningRate/2 end
  	
  	print(c.blue '==>'.." online epoch # " .. epoch .. ' [batchSize = ' .. opt.batchSize .. ']')

	local targets = cast(torch.DoubleTensor(opt.batchSize))
	local traindata_size = provider.TrainData:size() 
	local indices = torch.randperm(traindata_size):long():split(opt.batchSize)
	indices[#indices] = nil -- remove the last batch since in most time, this batch has different size 

	local tic = torch.tic()
	train_loss = 0.0
	for t, v in ipairs(indices) do 
		mp.info(string.format("Train error: %f", train_loss), 2)
		mp.progress(t, #indices, 1)

		provider:get_next_batch(v, 'train')
		local trainDataBatch = provider.TrainDataBatch

		local inputs = {{cast(trainDataBatch.data:select(1, 1)), 
						cast(trainDataBatch.pos:select(1, 1)), cast(trainDataBatch.ori:select(1, 1))}, 
						{cast(trainDataBatch.data:select(1, 2)), 
						cast(trainDataBatch.pos:select(1, 2)), cast(trainDataBatch.ori:select(1, 2))}}

		targets:copy(trainDataBatch.labels)

		local feval = function(x)
			if x ~= parameters then parameters:copy(x) end
			gradParameters:zero()

			local outputs = model:forward(inputs)
			local f = criterion:forward(outputs, targets)
			local df_do = criterion:backward(outputs, targets)
			model:backward(inputs, df_do)

			train_loss = (train_loss * (t-1) + f) / t

			return f, gradParameters
		end
		optim.sgd(feval, parameters, optimState)
	end
	mp.resetProgress()
	
 	print(('Train Loss: ' .. c.cyan '%.2f' .. ' \t time: %.2f s'):format(
 	      train_loss, torch.toc(tic)))

 	epoch = epoch + 1
end

function test()
	model:evaluate()

	print(c.blue '==> ' .. 'Testing ')

	local test_loss = 0.0
	local bs = 64
	local targets = cast(torch.DoubleTensor(bs))
	local testdata_size = provider.TestData:size()
	local indices = torch.randperm(testdata_size):long():split(bs)
	indices[#indices] = nil

	for t, v in ipairs(indices) do 
		mp.progress(t, #indices)

		provider:get_next_batch(v, 'test')
		local testDataBatch = provider.TestDataBatch

		local inputs = {{cast(testDataBatch.data:select(1, 1)), 
						cast(testDataBatch.pos:select(1, 1)), cast(testDataBatch.ori:select(1, 1))}, 
						{cast(testDataBatch.data:select(1, 2)), 
						cast(testDataBatch.pos:select(1, 2)), cast(testDataBatch.ori:select(1, 2))}}

		targets:copy(testDataBatch.labels)

		local outputs = model:forward(inputs)
		local f = criterion:forward(outputs, targets)
			
		test_loss = (test_loss * (t-1) + f) / t
	end
	mp.resetProgress()

	print(('Test loss: ' .. c.cyan '%.2f'):format(test_loss))

	if testLogger then 
		testLogger:add{train_loss, test_loss}
		testLogger:style{'+-', '+-'}
		testLogger:plot()

		if paths.filep(paths.concat(opt.save, 'test.log.eps')) then
			local base64im
      		do
				os.execute(('convert -density 200 %s/test.log.eps %s/test.png'):format(opt.save,opt.save))
      		  	os.execute(('openssl base64 -in %s/test.png -out %s/test.base64'):format(opt.save,opt.save))
      		  	local f = io.open(paths.concat(opt.save, 'test.base64'))
      		  	if f then base64im = f:read'*all' end
      		end
			
			local file = io.open(paths.concat(opt.save, 'report.html'),'w')
      		file:write(([[
      				<!DOCTYPE html>
      				<html>
      				<body>
      				<title>%s - %s</title>
      				<img src="data:image/png;base64,%s">
      				<h4>optimState:</h4>
      				<table>
      				]]):format(opt.save,epoch,base64im))

      		for k,v in pairs(optimState) do
      		  	if torch.type(v) == 'number' then
      		    	file:write('<tr><td>'..k..'</td><td>'..v..'</td></tr>\n')
      		  	end
			end
      		file:write'</table><pre>\n'
      		file:write(tostring(confusion)..'\n')
      		file:write(tostring(model)..'\n')
      		file:write'</pre></body></html>'
      		file:close()
		end
	end

	-- save model every epoch
	local filename = paths.concat(opt.save, 'history_models/model_' .. (epoch-1) .. '.net')
	print('==> saving model to ' .. filename)
	torch.save(filename, model)
end

for i=1,opt.max_epoch do 
	train()
	test()
end





