require 'nngraph';
require 'nn';
require 'cunn';
require 'lua-code/LIFT_model';
require 'image';
matio = require 'matio';

--- Initialize the network --- 
lift_weight_file = 'lua-code/LIFT_weight.mat'
lift_desc_weight_file = 'lua-code/LIFT_DESC_weight.mat'
lift_mean_std_file = 'lua-code/LIFT_mean_std.mat'

-- main modules for each 
detect_module, _ = model_detector(true, lift_weight_file):cuda()
detect_module:evaluate()
orient_module, _ = model_orientation(lift_weight_file):cuda()
orient_module:evaluate()
desc_module, _ = model_descriptor(lift_desc_weight_file):float()
desc_module:evaluate()

-- get the position of the keypoint from scoremap 
kp_pos_net_input = - nn.Identity()
kp_pos_net_output = kp_pos_net_input - nn.SoftArgMax() - nn.AddConstant(17)
kp_pos_net = nn.gModule({kp_pos_net_input}, {kp_pos_net_output}):cuda()
kp_pos_net:evaluate()

-- create crop from keypoint position 
crop_net_input_patch = - nn.Identity()
crop_net_input_pos = - nn.Identity()
crop_net_output = {crop_net_input_patch, crop_net_input_pos} - nn.CreateCrop(64, 64)
crop_net = nn.gModule({crop_net_input_patch, crop_net_input_pos}, {crop_net_output}):cuda()
crop_net:evaluate()

-- create crop from keypoint position and orientation 
cropRot_net_input_patch = - nn.Identity()
cropRot_net_input_pos = - nn.Identity()
cropRot_net_input_rot = - nn.Identity()
cropRot_net_output = {cropRot_net_input_patch, cropRot_net_input_pos, cropRot_net_input_rot} - nn.CreateCropRotate(64, 64)
cropRot_net = nn.gModule({cropRot_net_input_patch, cropRot_net_input_pos, cropRot_net_input_rot}, {cropRot_net_output}):cuda()
cropRot_net:evaluate()



--- Process input data --- 
filename = 'sample_patches/crop_2.jpg'
lift_mean_std = matio.load(lift_mean_std_file)
input = torch.Tensor(1, 1, 128, 128):copy((image.load(filename)*255.0 - lift_mean_std['mean'][1][1])/lift_mean_std['std'][1][1]):cuda()

score_map = detect_module:forward(input)
-- print(score_map)
score_map = score_map[{{}, {}, {5, 98}, {5, 98}}]
-- image.display{score_map} -- show score_map
kp_pos = kp_pos_net:forward(score_map)
print(kp_pos)
crop_for_ori = crop_net:forward({input, kp_pos})
-- image.display{crop_for_ori} -- show crop_for_ori 
kp_ori = orient_module:forward(crop_for_ori)
print(kp_ori)
crop_for_desc = cropRot_net:forward({input, kp_pos, kp_ori})

desc_weight = matio.load(lift_desc_weight_file)
crop_for_desc = (crop_for_desc*lift_mean_std['std'][1][1]+lift_mean_std['mean'][1][1] - desc_weight['lift_desc-patch_mean'][1][1]) / desc_weight['lift_desc-patch_std'][1][1]
-- image.display{crop_for_desc} -- show crop_for_desc 
kp_desc = desc_module:forward(crop_for_desc:float())
print(kp_desc)


