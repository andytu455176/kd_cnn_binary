require 'nngraph';
require 'LIFT_model';
require 'image';
require 'xlua';

local matio = require 'matio';

opt = lapp[[
   --image_list_name          (default None)     image list file name 
   --output_name              (default None)     output of the kpKpPosOri file name  
   --stat_name                (default None)     image list mean and std file name 
   --batch_size               (default 32)       batch size for running the network 
]]
print(opt)


function batch_patch_KpPosOri(score_map_model, full_model, image_table, index_start, index_end, mean, std) 
	local input = torch.DoubleTensor(index_end-index_start, 1, 128, 128):fill(0)

	for i=1,index_end-index_start do 
		local filename = image_table[i]
		local x = image.load(filename, 1, 'double')
		input:select(1, i):copy((x - mean) / std)
	end

	input = input:cuda()

	score_map_model:evaluate()
	local score_map = score_map_model:forward(input)
	score_map = score_map[{{}, {}, {5, 98}, {5, 98}}]

	full_model:evaluate()
	local output = full_model:forward({input, score_map})

	return output[1], output[2] -- kp_pos, kp_ori
end

function construct_model()
	local detect, detect_layers = model_detector(false, 'LIFT_weight.mat')
	local orient, orient_layers = model_orientation('LIFT_weight.mat')

	local score_map_net_input = -nn.Identity()
	local score_map_net_output = score_map_net_input - detect 
	local score_map_net = nn.gModule({score_map_net_input}, {score_map_net_output})
	score_map_net:evaluate()

	local net_input = -nn.Identity()
	local score_map_blob = -nn.Identity()
	local kp_pos = score_map_blob - nn.SoftArgMax() - nn.AddConstant(17.0)
	local crop_for_ori = {net_input, kp_pos} - nn.CreateCrop(64, 64)
	local kp_ori = crop_for_ori - orient
	-- local crop_for_desc = {net_input, kp_pos, kp_ori} - nn.CreateCropRotate(64, 64)
	
	local net = nn.gModule({net_input, score_map_blob}, {kp_pos, kp_ori})
	net:evaluate()

	return score_map_net, net 
end

function run(input_name, output_name, stat_name, batch_size)
	local image_table = {}

	-- load image_table from input_name 
	for line in io.lines(input_name) do 
		table.insert(image_table, line)
	end

	local cur_index = 1
	local score_map_model, full_model = construct_model()
	score_map_model = score_map_model:cuda()
	full_model = full_model:cuda()
	local mean, std 
	statistics = matio.load(stat_name)
	mean = statistics['mean'][1][1]
	std = statistics['std'][1][1]

	pos_data = torch.DoubleTensor(#image_table, 2):fill(0)
	ori_data = torch.DoubleTensor(#image_table, 1):fill(0)

	while cur_index <= #image_table do 
		xlua.progress(cur_index, #image_table)
		cur_batch = math.min(batch_size, #image_table-cur_index+1)

		local kp_pos, kp_ori = batch_patch_KpPosOri(score_map_model, full_model, 
					image_table, cur_index, cur_index+cur_batch, mean, std) 

		pos_data[{{cur_index, cur_index+cur_batch-1}, {}}]:copy(kp_pos:double())
		ori_data[{{cur_index, cur_index+cur_batch-1}, {}}]:copy(kp_ori:double())

		cur_index = cur_index + cur_batch 
	end
	matio.save(output_name, {kp_pos=pos_data, kp_ori=ori_data})
end

run(opt.image_list_name, opt.output_name, opt.stat_name, opt.batch_size)






---------- below is just for test --------- 
--score_map_model, full_model = construct_model()
--input = torch.DoubleTensor(2, 1, 128, 128):fill(0)
--
--filename = '/4t/andytu28/KD_CNN_Binary/data/Roman_result1/TrainData/pos_patches/3223917024_4c83d841a9_o/255837_06361.png'
--x = image.load(filename, 1, 'double') * 255.0
--input:select(1, 1):copy(x)
--
--filename = '/4t/andytu28/KD_CNN_Binary/data/Roman_result1/TrainData/pos_patches/2753753124_f281fcae01_o/105407_06722.png'
--x = image.load(filename, 1, 'double') * 255.0
--input:select(1, 2):copy(x)
--
--input = input:cuda()
--score_map_model = score_map_model:cuda()
--full_model = full_model:cuda()
--
--score_map = score_map_model:forward(input)
--score_map = score_map[{{}, {}, {5,98}, {5,98}}]
--
--output = full_model:forward({input, score_map})
--
--output[1] = output[1]:double()
--
--print(output[2])
--print(output[3])
--
--image.display(output[1][1])














