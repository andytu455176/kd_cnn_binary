require 'nn';
require 'nngraph';
require 'cunn';

local matio = require 'matio';

function model_siamese_desc(init_weight)
	--------------------------------
	-- For training descriptor only, construct the siamese network for desc part 
	--
	
	local desc, desc_layers = model_descriptor(init_weight)
	local main_model = nn.Sequential()
	main_model:add(nn.CreateCropRotate(64, 64))
	main_model:add(desc)

	local parallel = nn.ParallelTable() -- expect ({{data1, pos1, ori1}, {data1, pos1, ori1}})
	parallel:add(main_model)
	parallel:add(main_model:clone('weight', 'bias', 'gradWeight', 'gradBias'))

	local siamese = nn.Sequential()
	siamese:add(parallel)
	siamese:add(nn.PairwiseDistance(2))

	--repair_SpatialSubtractiveNormalization_clone(siamese:get(1):get(2):get(2):get(4))
	--repair_SpatialSubtractiveNormalization_clone(siamese:get(1):get(2):get(2):get(8))

	return siamese
end

function model_siamese(init_weight)
	local main_model = model_combine(init_weight)

	local parallel = nn.ParallelTable() -- expect (2xNxCxHxW)
	parallel:add(main_model)
	parallel:add(main_model:clone('weight', 'bias', 'gradWeight', 'gradBias'))

	local siamese = nn.Sequential()
	siamese:add(nn.SplitTable(1))
	siamese:add(parallel)
	siamese:add(nn.PairwiseDistance(2))

	repair_SpatialSubtractiveNormalization_clone(siamese:get(2):get(2):get(1):get(3):get(4))
	repair_SpatialSubtractiveNormalization_clone(siamese:get(2):get(2):get(1):get(3):get(8))

	local margin = 1
	local criterion = nn.HingeEmbeddingCriterion(margin)
	return siamese, criterion
end

function repair_SpatialSubtractiveNormalization_clone(module)
	local kdim = module.kernel:nDimension()

	local padH = math.floor(module.kernel:size(1)/2)
	local padW = padH
	if kdim == 2 then
	   padW = math.floor(module.kernel:size(2)/2)
	end
	
	module.meanestimator = nn.Sequential()
	module.meanestimator:add(nn.SpatialZeroPadding(padW, padW, padH, padH))
	if kdim == 2 then
	   module.meanestimator:add(nn.SpatialConvolution(module.nInputPlane, 1, module.kernel:size(2), module.kernel:size(1)))
	else
	   module.meanestimator:add(nn.SpatialConvolutionMap(nn.tables.oneToOne(module.nInputPlane), module.kernel:size(1), 1))
	   module.meanestimator:add(nn.SpatialConvolution(module.nInputPlane, 1, 1, module.kernel:size(1)))
	end
	module.meanestimator:add(nn.Replicate(module.nInputPlane,1,3))

   	if kdim == 2 then
		for i = 1,module.nInputPlane do 
			module.meanestimator.modules[2].weight[1][i] = module.kernel
		end
		module.meanestimator.modules[2].bias:zero()
	else
		for i = 1,module.nInputPlane do 
			module.meanestimator.modules[2].weight[i]:copy(module.kernel)
			module.meanestimator.modules[3].weight[1][i]:copy(module.kernel)
		end
		module.meanestimator.modules[2].bias:zero()
		module.meanestimator.modules[3].bias:zero()
	end
end

function model_combine(init_weight)
	local detect, detect_layers
	detect, detect_layers = model_detector(false, init_weight)
	local orient, orient_layers 
	orient, orient_layers = model_orientation(init_weight)
	local desc, desc_layers
	desc, desc_layers = model_descriptor(init_weight)

	local detect_module = nn.Sequential()
	detect_module:add(detect)
	detect_module:add(nn.SoftArgMax())

	local ori_input = nn.ConcatTable()
	ori_input:add(nn.Identity())
	ori_input:add(detect_module)

	local ori_module = nn.Sequential()
	ori_module:add(ori_input)
	ori_module:add(nn.CreateCrop(64, 64))
	ori_module:add(orient)

	local desc_input = nn.ConcatTable()
	desc_input:add(nn.Identity())
	desc_input:add(detect_module)
	desc_input:add(ori_module)

	local desc_module = nn.Sequential()
	desc_module:add(desc_input)
	desc_module:add(nn.CreateCropRotate(64, 64))
	desc_module:add(desc)

	local main_model = nn.Sequential()
	main_model:add(desc_module)

	return main_model
end

function model_combine_nngraph()
	local detect, detect_layers
	detect, detect_layers = model_detector()
	local orient, orient_layers 
	orient, orient_layers = model_orientation()
	local desc, desc_layers
	desc, desc_layers = model_descriptor()

	local input_patch = - nn.Identity()
	local score_map = input_patch - detect
	local kp_pos = score_map - nn.SoftArgMax()
	local crop_patch = {input_patch, kp_pos} - nn.CreateCrop(64, 64)
	local kp_ori = crop_patch - orient 
	local crop_rot_patch = {input_patch, kp_pos, kp_ori} - nn.CreateCropRotate(64, 64)
	local kp_desc = crop_rot_patch - desc 

	local net_input = {input_patch}
	local net_output = {kp_pos, kp_ori, kp_desc}

	return net_input, net_output
end

function model_detector(is_gray, init_weight)
	is_gray = is_gray or true 
	local detect_layers = {}
	if not is_gray then
		detect_layers['lift_pos-rgb2gray'] = nn.SpatialConvolution(3, 1, 1, 1)
	end
	detect_layers['lift_pos-conv1'] = nn.SpatialConvolution(1, 16, 25, 25)

	local detect = nn.Sequential()
	if not is_gray then 
		detect:add(detect_layers['lift_pos-rgb2gray'])
	end
	detect:add(detect_layers['lift_pos-conv1'])
	detect:add(nn.GHHPooling(1, 4, 4))

	if init_weight then
		weight_table = matio.load(init_weight)
		if not is_gray then 
			detect_layers['lift_pos-rgb2gray'].weight[{{1}, {}, {1}, {1}}] = torch.DoubleTensor({0.299, 0.587, 0.114})
		end
		assert(detect_layers['lift_pos-conv1'].weight:isSize(weight_table['lift_pos-conv1.W']:size()))
		assert(detect_layers['lift_pos-conv1'].bias:isSize(weight_table['lift_pos-conv1.b']:view(-1):size()))
		detect_layers['lift_pos-conv1'].weight:copy(weight_table['lift_pos-conv1.W'])
		detect_layers['lift_pos-conv1'].bias:copy(weight_table['lift_pos-conv1.b']:view(-1))
	end

	return detect, detect_layers 
end

function model_orientation(init_weight)
	local orient_layers = {}
	orient_layers['lift_ori-conv1'] = nn.SpatialConvolution(1, 10, 5, 5)
	orient_layers['lift_ori-conv2'] = nn.SpatialConvolution(10, 20, 5, 5)
	orient_layers['lift_ori-conv3'] = nn.SpatialConvolution(20, 50, 3, 3)
	orient_layers['lift_ori-fc4'] = nn.Linear(6050, 4 * 4 * 100)
	orient_layers['lift_ori-fc5'] = nn.Linear(100, 4 * 4 * 2)

	local orient = nn.Sequential()
	orient:add(orient_layers['lift_ori-conv1'])
	orient:add(nn.ReLU())
	orient:add(nn.SpatialMaxPooling(2, 2))

	orient:add(orient_layers['lift_ori-conv2'])
	orient:add(nn.ReLU())
	orient:add(nn.SpatialMaxPooling(2, 2))

	orient:add(orient_layers['lift_ori-conv3'])
	orient:add(nn.ReLU())
	-- orient:add(nn.SpatialMaxPooling(2, 2))

	orient:add(nn.Reshape(6050, true))
	orient:add(orient_layers['lift_ori-fc4'])
	orient:add(nn.GHHPooling(100, 4, 4))
	orient:add(nn.Dropout(0.3))

	orient:add(orient_layers['lift_ori-fc5'])
	orient:add(nn.GHHPooling(2, 4, 4))
	orient:add(nn.Atan2())

	if init_weight then
		weight_table = matio.load(init_weight)
		for k, v in pairs(orient_layers) do 
			local target_weight, target_bias
			if k:sub(10, 11) == 'fc' then 
				target_weight = weight_table[k..'.W']:transpose(1, 2)
				target_bias = weight_table[k..'.b']:view(-1)
			elseif k:sub(10, 13) == 'conv' then 
				target_weight = weight_table[k..'.W']
				target_bias = weight_table[k..'.b']:view(-1)
			end
			assert(orient_layers[k].weight:isSize(target_weight:size()))
			assert(orient_layers[k].bias:isSize(target_bias:size()))
			orient_layers[k].weight:copy(target_weight)
			orient_layers[k].bias:copy(target_bias)
		end
	end

	return orient, orient_layers
end

function model_descriptor(init_weight)
	local desc_layers = {}
	desc_layers['lift_desc-conv1'] = nn.SpatialConvolution(1, 32, 7, 7)
	desc_layers['lift_desc-conv2'] = nn.SpatialConvolution(32, 64, 6, 6)
	desc_layers['lift_desc-conv3'] = nn.SpatialConvolution(64, 128, 5, 5)

	local desc = nn.Sequential()
	desc:add(desc_layers['lift_desc-conv1'])
	desc:add(nn.Tanh())
	desc:add(nn.SpatialLPPooling(32, 2, 2, 2))
	if init_weight then 
		local weight_table = matio.load(init_weight)
		desc:add(nn.SpatialSubtractiveNormalization(32, weight_table['lift_desc-subt_norm1']))
	else
		desc:add(nn.SpatialSubtractiveNormalization(32, torch.ones(5)))
	end

	desc:add(desc_layers['lift_desc-conv2'])
	desc:add(nn.Tanh())
	desc:add(nn.SpatialLPPooling(64, 2, 3, 3))
	if init_weight then 
		local weight_table = matio.load(init_weight)
		desc:add(nn.SpatialSubtractiveNormalization(64, weight_table['lift_desc-subt_norm2']))
	else
		desc:add(nn.SpatialSubtractiveNormalization(64, torch.ones(5)))
	end

	desc:add(desc_layers['lift_desc-conv3'])
	desc:add(nn.Tanh())
	desc:add(nn.SpatialLPPooling(128, 2, 4, 4))
	desc:add(nn.View(-1):setNumInputDims(3))

	if init_weight then
		local weight_table = matio.load(init_weight)
		for k, v in pairs(desc_layers) do 
			assert(desc_layers[k].weight:isSize(weight_table[k..'.W']:size()))
			assert(desc_layers[k].bias:isSize(weight_table[k..'.b']:view(-1):size()))
			desc_layers[k].weight:copy(weight_table[k..'.W'])
			desc_layers[k].bias:copy(weight_table[k..'.b']:view(-1))
		end
	end

	return desc, desc_layers
end













