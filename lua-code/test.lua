require 'LIFT_model';
require 'nngraph';
require 'image';

----- (1) Get model Structure ----- 
--net_input, net_output = model_combine()
--
--kd_cnn = nn.gModule(net_input, net_output)
--
------- (2) Data Processing ----- 
--filename = '/4t/andytu28/2367190890_65f5afc918_o/205128_01790.png'
--x = image.load(filename, 1, 'double')
--input = torch.DoubleTensor(2, x:size(1), x:size(2), x:size(3))
--input:select(1, 1):copy(x)
--input:select(1, 2):copy(x)
--
---- input = input:cuda()
---- kd_cnn = kd_cnn:cuda()
--
--output = kd_cnn:forward(input)
--print(output)
--
------- end of the test file ----------

----- (1) Get model Structure ----- 
---- kd_cnn = model_combine()
--kd_cnn, _ = model_siamese('LIFT_weight.mat')
--print(kd_cnn)
--
------- (2) Data Processing ----- 
--filename = '/4t/andytu28/KD_CNN_Binary/data/Roman_result1/TrainData/pos_patches/2753753124_f281fcae01_o/105407_06722.png'
--x = image.load(filename, 1, 'double')
--input = torch.DoubleTensor(2, 1, x:size(1), x:size(2), x:size(3))
---- input = torch.DoubleTensor(2, x:size(1), x:size(2), x:size(3))
--input:select(1, 1):copy(x)
--input:select(1, 2):copy(x)
--
---- input = input:cuda()
---- kd_cnn = kd_cnn:cuda()
--
--output = kd_cnn:forward(input)
--print(output:size())
--
------- end of the test file ----------

----- (1) Get model Structure ----- 

kd_cnn = model_siamese_desc('LIFT_weight.mat')
print(kd_cnn)

----- (2) Data Processing ----- 
filename = '/4t/andytu28/KD_CNN_Binary/data/Roman_result1/TrainData/pos_patches/2753753124_f281fcae01_o/105407_06722.png'
x = image.load(filename, 1, 'double')
input = torch.DoubleTensor(2, 1, x:size(1), x:size(2), x:size(3))
-- input = torch.DoubleTensor(2, x:size(1), x:size(2), x:size(3))
input:select(1, 1):copy(x)
input:select(1, 2):copy(x)

pos = torch.DoubleTensor(2, 1, 2):fill(0)
ori = torch.DoubleTensor(2, 1, 1):fill(0)

input = input:cuda()
pos = pos:cuda()
ori = ori:cuda()

kd_cnn = kd_cnn:cuda()

network_input = {{input:select(1, 1), pos:select(1, 1), ori:select(1, 1)},
                 {input:select(1, 2), pos:select(1, 2), ori:select(1, 2)}}

output = kd_cnn:forward(network_input)

print(output)








