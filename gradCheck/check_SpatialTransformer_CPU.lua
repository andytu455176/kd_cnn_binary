local SpatialTransformerTest = torch.TestSuite()
local mytester = torch.Tester()
local jac
local sjac

local precision = 1e-5
local expprecision = 1e-4

function SpatialTransformerTest.Backward_dU()
	local batch_size = torch.random(2, 10)
	local height = torch.random(3, 7)
	local width = torch.random(3, 7)
	local channel = torch.random(1, 3)

	local images = torch.randn(batch_size, channel, height, width)
	local params = torch.zeros(batch_size, 3, 2):uniform(-math.pi, math.pi)

	local output_H = torch.random(3, 5)
	local output_W = torch.random(3, 5)

	local module = nn.SpatialTransformer(output_H, output_W)

	module._updateOutput = module.updateOutput
	function module:updateOutput(input)
		return self:_updateOutput({input, params})
	end

	module._updateGradInput = module.updateGradInput
	function module:updateGradInput(input, gradOutput)
		self:_updateGradInput({input, params}, gradOutput)
		return self.gradInput[1]
	end

	local errImages = jac.testJacobian(module, images:contiguous())
	mytester:assertlt(errImages, precision, 'error on state ')
end

function SpatialTransformerTest.Backward_dTheta()
	local batch_size = torch.random(2, 10)
	local height = torch.random(3, 7)
	local width = torch.random(3, 7)
	local channel = torch.random(1, 3)

	local images = torch.randn(batch_size, channel, height, width)
	local params = torch.zeros(batch_size, 3, 2):uniform(-math.pi, math.pi)

	local output_H = torch.random(3, 5)
	local output_W = torch.random(3, 5)
	local module = nn.SpatialTransformer(output_H, output_W)

	module._updateOutput = module.updateOutput
	function module:updateOutput(input)
		return self:_updateOutput({images, input})
	end

	module._updateGradInput = module.updateGradInput
	function module:updateGradInput(input, gradOutput)
		self:_updateGradInput({images, input}, gradOutput)
		return self.gradInput[2]
	end

	local errParams = jac.testJacobian(module, params:contiguous())
	mytester:assertlt(errParams, precision, 'error on state ')
end

require 'nn'
mytester:add(SpatialTransformerTest)
jac = nn.Jacobian
sjac = nn.SparseJacobian
mytester:run()

