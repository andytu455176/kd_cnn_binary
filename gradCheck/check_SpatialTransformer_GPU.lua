local SpatialTransformerTest = torch.TestSuite()
local mytester = torch.Tester()

local precision_forward = 1e-4
local precision_backward = 1e-2

local typenames = {
  'torch.CudaTensor',
  'torch.CudaDoubleTensor',
}

local t2cpu = {
  ['torch.CudaTensor'] = 'torch.FloatTensor',
  ['torch.CudaDoubleTensor'] = 'torch.DoubleTensor',

}

local function checkHalf()
   if cutorch.hasHalf then
       table.insert(typenames, 'torch.CudaHalfTensor')
       t2cpu['torch.CudaHalfTensor'] = 'torch.FloatTensor'
   end
end

local function makeNonContiguous(tensor)
   size = tensor:size()
   local osize = {}
   for i = 1, #size do osize[i] = size[i] end
   -- randomly inflate a few dimensions in osize
   for i = 1, 3 do
      local dim = torch.random(1,#osize)
      local add = torch.random(4, 15)
      osize[dim] = osize[dim] + add
   end
   local input = torch[tensor:type():match('torch.(%a+)')]()
   input:resize(torch.LongStorage(osize))
   -- now extract the input of correct size from 'input'
   for i = 1, #size do
      if input:size(i) ~= size[i] then
         local bounds = torch.random(1, input:size(i) - size[i] + 1)
         input = input:narrow(i, bounds, size[i])
      end
   end
   input:copy(tensor)
   return input
end

-- half has additional error on top of double/float
local function precision_forward_type(precision_f, tensor_type, maxabs)
   if (tensor_type == 'torch.CudaHalfTensor') then
      return 1e-2 + precision_f + half_max_error(maxabs)
   else
      return precision_f
   end
end

local function precision_backward_type(precision_b, tensor_type, maxabs)
   if (tensor_type == 'torch.CudaHalfTensor') then
      return 1e-1 + precision_b + half_max_error(maxabs)
   else
      return precision_b
   end
end

function SpatialTransformerTest.Forward()
	local batch_size = torch.random(2, 10)
	local height = torch.random(3, 7)
	local width = torch.random(3, 7)
	local channel = torch.random(1, 3)
	local output_H = torch.random(3, 5)
	local output_W = torch.random(3, 5)

	local module = nn.SpatialTransformer(output_H, output_W)

	for k, typename in ipairs(typenames) do 
		local images = torch.randn(batch_size, channel, height, width)
		local params = torch.zeros(batch_size, 3, 2):uniform(-math.pi, math.pi)

		local ctype = t2cpu[typename]
		images = makeNonContiguous(images:type(ctype))
		params = makeNonContiguous(params:type(ctype))
		local cspt = nn.SpatialTransformer(output_H, output_W):type(ctype)
		local groundtruth = cspt:forward({images, params})

		images = makeNonContiguous(images:type(typename))
		params = makeNonContiguous(params:type(typename))
		local gspt = nn.SpatialTransformer(output_H, output_W):type(typename)
		local rescuda = gspt:forward({images, params})

      	local error = rescuda:double() - groundtruth:double()
		mytester:assertlt(error:abs():max(), precision_forward_type(precision_forward, typename),
			string.format('error on state (forward) with %s', typename))
		mytester:assert(groundtruth:isSize(rescuda:size()),
			string.format('size mismatch on state (forward) with %s', typename))
	end
end

function SpatialTransformerTest.Backward()
	local batch_size = torch.random(2, 10)
	local height = torch.random(3, 7)
	local width = torch.random(3, 7)
	local channel = torch.random(1, 3)
	local output_H = torch.random(3, 5)
	local output_W = torch.random(3, 5)

	local module = nn.SpatialTransformer(output_H, output_W)

	for k, typename in ipairs(typenames) do 
		local images = torch.randn(batch_size, channel, height, width)
		local params = torch.zeros(batch_size, 3, 2):uniform(-math.pi, math.pi)
		local gradOutput = torch.randn(batch_size, channel, output_H, output_W)

		local ctype = t2cpu[typename]
		images = makeNonContiguous(images:type(ctype))
		params = makeNonContiguous(params:type(ctype))
		gradOutput = makeNonContiguous(gradOutput:type(ctype))
		local cspt = nn.SpatialTransformer(output_H, output_W):type(ctype)
		cspt:forward({images, params})
		cspt:zeroGradParameters()
		local groundgrad = cspt:backward({images, params}, gradOutput)

		images = makeNonContiguous(images:type(typename))
		params = makeNonContiguous(params:type(typename))
		gradOutput = makeNonContiguous(gradOutput:type(typename))
		local gspt = nn.SpatialTransformer(output_H, output_W):type(typename)
		gspt:forward({images, params})
		gspt:zeroGradParameters()
		local rescuda = gspt:backward({images, params}, gradOutput)

      	local gradImagesError = rescuda[1]:double() - groundgrad[1]:double()
		local gradParamsError = rescuda[2]:double() - groundgrad[2]:double()

		mytester:assertlt(gradImagesError:abs():max(), precision_forward_type(precision_forward, typename),
			string.format('error on state (forward) with %s', typename))
		mytester:assert(groundgrad[1]:isSize(rescuda[1]:size()),
			string.format('size mismatch on state (forward) with %s', typename))

		mytester:assertlt(gradParamsError:abs():max(), precision_forward_type(precision_forward, typename),
			string.format('error on state (forward) with %s', typename))
		mytester:assert(groundgrad[2]:isSize(rescuda[2]:size()),
			string.format('size mismatch on state (forward) with %s', typename))
	end
end

require 'cunn'
torch.setdefaulttensortype('torch.FloatTensor')
-- checkHalf()
mytester:add(SpatialTransformerTest)
mytester:run()


