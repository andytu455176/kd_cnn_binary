local GHHPoolingTest = torch.TestSuite()
local mytester = torch.Tester()
local jac
local sjac

local precision = 1e-5
local expprecision = 1e-4

function GHHPoolingTest.Backward_1D()
	local batch_size = torch.random(2, 10)
	local channel = 30 
	local images = torch.zeros(batch_size, channel):uniform()
	local param = torch.Tensor{2, 3, 5}
	local param_index = torch.randperm(3)
	
	local nu = param[param_index[1]]
	local ns = param[param_index[2]]
	local nm = param[param_index[3]]
	local module = nn.GHHPooling(nu, ns, nm)

	local errImages = jac.testJacobian(module, images)
	mytester:assertlt(errImages, precision, 'error on state ')
end

function GHHPoolingTest.Backward_2D()
	local batch_size = torch.random(2, 10)
	local channel = 30 
	local height = torch.random(5, 10)
	local width = torch.random(5, 10)
	local images = torch.zeros(batch_size, channel, height, width):uniform()
	local param = torch.Tensor{2, 3, 5}
	local param_index = torch.randperm(3)

	local nu = param[param_index[1]]
	local ns = param[param_index[2]]
	local nm = param[param_index[3]]
	local module = nn.GHHPooling(nu, ns, nm)

	local errImages = jac.testJacobian(module, images)
	mytester:assertlt(errImages, precision, 'error on state ')
end

require 'nn'
mytester:add(GHHPoolingTest)
jac = nn.Jacobian
sjac = nn.SparseJacobian
mytester:run()

