require 'paths';
require 'image';
require 'xlua';
local c = require 'trepl.colorize';
local matio = require 'matio';

local Provider = torch.class('Provider')
train_image_list = 'dataset_list/TrainImages.txt'
train_data_list = 'dataset_list/TrainData.mat'
test_image_list = 'dataset_list/TestImages.txt'
test_data_list = 'dataset_list/TestData.mat'
train_batch_size = 128
test_batch_size = 64

function Provider:__init()

	---------- For TrainData ---------- 
	self.TrainImages = {
		image_names = {}
	}

	print(c.blue '==> ' .. 'Provdier __init() ... load train_image_list ... ')
	for line in io.lines(train_image_list) do 
		table.insert(self.TrainImages.image_names, line)
	end

	print(c.blue '==> ' .. 'Provdier __init() ... load TrainData ... ')
	local train_data = matio.load(train_data_list)
	local train_data_size = train_data['pairs']:size(1)
	-- local train_data_size = 100
	self.TrainData = {
		data = train_data['pairs'], -- [{{1,100}}], 
		labels = train_data['labels'], -- [{{}, {1,100}}], 
		size = function() return train_data_size end 
	}
	self.TrainDataBatch = {
		data = torch.Tensor(2, train_batch_size, 1, 128, 128), 
		labels = torch.Tensor(train_batch_size), 
		size = function() return train_batch_size end, 

		-- For training descriptor, need keypoints pos and ori as inputs 
		pos = torch.Tensor(2, train_batch_size, 2), 
		ori = torch.Tensor(2, train_batch_size, 1)
	}

	---------- For TestData ---------- 
	self.TestImages = {
		image_names = {}
	}

	print(c.blue '==> ' .. 'Provider __init() ... load test_image_list ... ')
	for line in io.lines(test_image_list) do 
		table.insert(self.TestImages.image_names, line)
	end

	print(c.blue '==> ' .. 'Provider __init() ... load TestData ... ')
	local test_data = matio.load(test_data_list)
	local test_data_size = test_data['pairs']:size(1)
	-- local test_data_size = 50 
	self.TestData = {
		data = test_data['pairs'], -- [{{1, 50}}], 
		labels = test_data['labels'], -- [{{}, {1, 50}}], 
		size = function() return test_data_size end 
	}
	self.TestDataBatch = {
		data = torch.Tensor(2, test_batch_size, 1, 128, 128), 
		labels = torch.Tensor(test_batch_size), 
		size = function() return test_batch_size end, 

		-- For testing descriptor, need keypoints pos and ori as inputs 
		pos = torch.Tensor(2, test_batch_size, 2), 
		ori = torch.Tensor(2, test_batch_size, 1)
	}

end

function Provider:calculate_mean_std(mean_std_file)
	----------------------------------------------
	-- Calculate mean and std for the TrainImages for normalization 
	--

	if mean_std_file then 
		if paths.filep(mean_std_file) then
			print(c.blue '==> ' .. 'Load mean and std from file "' .. mean_std_file .. '"')
			local mean_std = matio.load(mean_std_file)
			self.TrainImages.mean = mean_std['mean'][1][1]
			self.TrainImages.std = mean_std['std'][1][1]
		else
			error('Target mean_std_file "' .. mean_std_file .. '" not exists')
		end
	else
		print(c.blue '==> ' .. 'Calculate mean and std ... ')
		local trainImages = self.TrainImages.image_names
		local sum1, sum2 = 0.0, 0.0
		for i, v in ipairs(trainImages) do 
			xlua.progress(i, #trainImages)
			-- note that the size of all images are the same (128*128)
			local im = image.load(v, 1, 'double')
			sum1 = sum1 + im:mean()
			sum2 = sum2 + torch.pow(im, 2):mean()
		end 
		trainImages.mean = sum1 / #trainImages 
		trainImages.std = math.sqrt((sum2 - 2 * trainImages.mean * sum1) / #trainImages + math.pow(trainImages.mean,2))
	end
end

function Provider:load_KpPosOri(kp_pos_ori_file, target)
	---------------------------------- 
	-- If only train the descriptor, 
	-- then call this function first to load the pre-computed keypoint pos and ori
	--
	
	if not kp_pos_ori_file then 
		error('Have to provider the kp_pos_ori_file!')
	end

	if paths.filep(kp_pos_ori_file) then
		print(c.blue '==> ' .. 'Load keypoint pos and ori from file "' .. kp_pos_ori_file .. '"')

		local targetImages

		if target == 'train' then 
			targetImages = self.TrainImages
		elseif target == 'test' then 
			targetImages = self.TestImages
		else
			error('Target mode "' .. target .. '" is not supported')
		end

		local kp_pos_ori = matio.load(kp_pos_ori_file)
		targetImages.pos = kp_pos_ori['kp_pos']
		targetImages.ori = kp_pos_ori['kp_ori']
	else
		error('Target kp_pos_ori_file "' .. kp_pos_ori_file .. '" not exists')
	end
end

function Provider:get_next_batch(indices, target)
	---------------------------------
	-- Since the data size is huge, so need to re-load all the data into memory 
	--
	
	local targetImages, targetData, targetDataBatch
	local mean, std = self.TrainImages.mean, self.TrainImages.std 

	if target == 'train' then 
		targetImages = self.TrainImages 
		targetData = self.TrainData 
		targetDataBatch = self.TrainDataBatch
	elseif target == 'test' then 
		targetImages = self.TestImages 
		targetData = self.TestData 
		targetDataBatch = self.TestDataBatch
	else 
		error('Target mode "' .. target .. '" is not supported')
	end

	targetDataBatch.data:fill(0)
	targetDataBatch.labels:fill(0)
	targetDataBatch.pos:fill(0)
	targetDataBatch.ori:fill(0)

	for i=1,indices:size(1) do 
		local index1 = targetData.data[indices[i]][1]
		local index2 = targetData.data[indices[i]][2]
		local x = image.load(targetImages.image_names[index1], 1, 'double')
		targetDataBatch.data[{{1}, {i}}]:copy((x - mean) / std)
		x = image.load(targetImages.image_names[index2], 1, 'double')
		targetDataBatch.data[{{2}, {i}}]:copy((x - mean) / std)
		targetDataBatch.labels[i] = targetData.labels[1][indices[i]]

		-- For training or testing descriptor, need keypoints pos and ori as inputs 
		targetDataBatch.pos[{{1}, {i}}]:copy(targetImages.pos[{{index1}}])
		targetDataBatch.pos[{{2}, {i}}]:copy(targetImages.pos[{{index2}}])

		targetDataBatch.ori[{{1}, {i}}]:copy(targetImages.ori[{{index1}}])
		targetDataBatch.ori[{{2}, {i}}]:copy(targetImages.ori[{{index2}}])
	end
end












