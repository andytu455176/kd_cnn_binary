local CreateCropRotate, parent = torch.class('nn.CreateCropRotate', 'nn.Module')

function CreateCropRotate:__init(output_H_, output_W_)
	parent.__init(self)
	self.output_H = output_H_
	self.output_W = output_W_
	self.gradInput = {}
	self.gradInput[1] = torch.DoubleTensor()
	self.gradInput[2] = torch.DoubleTensor()
	self.gradInput[3] = torch.DoubleTensor()

	-- single patch_mask 
	self.single_patch_mask = torch.DoubleTensor(self.output_H*self.output_W, 3)
	for i=1,self.output_H do 
		for j=1,self.output_W do 
			self.single_patch_mask[(i-1)*self.output_W + j][1] = (i-1) - self.output_H / 2
			self.single_patch_mask[(i-1)*self.output_W + j][2] = (j-1) - self.output_W / 2
			self.single_patch_mask[(i-1)*self.output_W + j][3] = 1
		end
	end

	-- batch mask 
	self.patch_mask = torch.DoubleTensor(1, self.output_H*self.output_W, 3)
	self.patch_mask:copy(self.single_patch_mask)
	self.transformed_mask = torch.DoubleTensor(1, self.output_H*self.output_W, 2):fill(0)

	-- batch coor_range 
	self.coor_range = torch.DoubleTensor(1, self.output_H, self.output_W, 4)

	-- param_buf 
	self.param_buf = torch.DoubleTensor() 
end

function CreateCropRotate:check(input, gradOutput)
	-- input[1] should be the input feature maps, input[2] should be the transformation parameters 
	assert(input[1]:size(1) == input[2]:size(1)) -- the number of batch should be the same 
	assert(input[1]:size(1) == input[3]:size(1)) -- the number of batch should be the same 
	assert(input[2]:size(2) == 2) -- the second input is the crop location with (x, y)  
	assert(input[3]:size(2) == 1) -- the third input is the crop rotation with (theta (in rad))
end

function CreateCropRotate:updateOutput(input)
	assert(input[1].THNN, torch.type(input[1])..'.THNN backend not imported')
	self:check(input)

	local batch_size = input[1]:size(1)
	if batch_size ~= self.patch_mask:size(1) then 
		self.patch_mask:resize(batch_size, self.output_H*self.output_W, 3)
		for i=1,batch_size do 
			self.patch_mask:select(1, i):copy(self.single_patch_mask)
		end
	end

	self.param_buf:resize(batch_size, 3, 2):fill(0)
	self.param_buf[{{}, {1}, {1}}]:copy(torch.cos(input[3]))
	self.param_buf[{{}, {2}, {1}}]:copy(torch.sin(input[3]))
	self.param_buf[{{}, {1}, {2}}]:copy(-torch.sin(input[3]))
	self.param_buf[{{}, {2}, {2}}]:copy(torch.cos(input[3]))
	self.param_buf:select(2, 3):copy(input[2])

	input[1].THNN.SpatialTransformer_updateOutput(
		input[1]:cdata(), 
		self.output:cdata(), 
		self.param_buf:cdata(), 
		self.patch_mask:cdata(), 
		self.transformed_mask:cdata(), 
		self.coor_range:cdata(), 
		self.output_H, self.output_W
	)
	return self.output
end

function CreateCropRotate:updateGradInput(input, gradOutput)
	assert(input[1].THNN, torch.type(input[1])..'.THNN backend not imported')
	self:check(input, gradOutput)

	input[1].THNN.SpatialTransformer_updateGradInput(
		input[1]:cdata(), 
		gradOutput:cdata(), 
		self.gradInput[1]:cdata(), 
		self.param_buf:cdata(), -- just use param_buf for gradInput_buf, since param is not used in calc gradInput 
		self.transformed_mask:cdata(), 
		self.coor_range:cdata(), 
		self.patch_mask:cdata(), 
		self.output_H, self.output_W
	)
	self.gradInput[2]:resizeAs(input[2])
	self.gradInput[2]:copy(self.param_buf:select(2, 3))

	self.gradInput[3]:resizeAs(input[3]):fill(0)
	self.gradInput[3] = self.gradInput[3] + torch.cmul(-torch.sin(input[3]), self.param_buf[{{}, {1}, {1}}])
	self.gradInput[3] = self.gradInput[3] + torch.cmul( torch.cos(input[3]), self.param_buf[{{}, {2}, {1}}])
	self.gradInput[3] = self.gradInput[3] + torch.cmul(-torch.cos(input[3]), self.param_buf[{{}, {1}, {2}}])
	self.gradInput[3] = self.gradInput[3] + torch.cmul(-torch.sin(input[3]), self.param_buf[{{}, {2}, {2}}])
	return self.gradInput
end

function CreateCropRotate:reset()
end








