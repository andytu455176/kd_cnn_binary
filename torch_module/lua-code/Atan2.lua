local Atan2, parent = torch.class('nn.Atan2', 'nn.Module')

function Atan2:__init()
	parent.__init(self)
end

function Atan2:check(input, gradOutput)
	assert(input:size(2) == 2) -- Atan2(y, x) along the channel axis 
end

function Atan2:updateOutput(input)
	assert(input.THNN, torch.type(input)..'.THNN backend not imported')
	self:check(input)

	local axis = 2
	local output_shape = input:size()
	output_shape[axis] = 1
	self.output:resize(output_shape)
	self.output:copy(torch.atan2(input[{{}, {1}}], input[{{}, {2}}]))
	return self.output
end

function Atan2:updateGradInput(input, gradOutput)
	self.gradInput:resizeAs(input):fill(0)
	return self.gradInput
end

function Atan2:reset()
end







