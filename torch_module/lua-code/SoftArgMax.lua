local SoftArgMax, parent = torch.class('nn.SoftArgMax', 'nn.Module')

function SoftArgMax:__init(soft_strength)
	parent.__init(self)
	soft_strength = soft_strength or 1.0 
	self.beta = soft_strength
end

function SoftArgMax:check(input, gradOutput)
	assert(input:size(2) == 1) -- consider x, y for now (TODO: may need z for the scale in the testing phase)
	assert(self.beta > 0) -- consider pure soft for now (< 0 means hardmax in LIFT implementation)
end

function SoftArgMax:updateOutput(input)
	assert(input.THNN, torch.type(input)..'.THNN backend not imported')
	self:check(input)
	self.output:resize(input:size(1), 2)
	local x_range = torch.repeatTensor(torch.range(0, input:size(3)-1):view(input:size(3), 1), input:size(1), 1, 1, input:size(4))
	local y_range = torch.repeatTensor(torch.range(0, input:size(4)-1):view(1, input:size(4)), input:size(1), 1, input:size(3), 1)
	if torch.type(self.output) == 'torch.CudaTensor' then 
		x_range = x_range:cuda()
		y_range = y_range:cuda()
	end

	local max_val 
	max_val, _ = torch.max(input, 3)
	max_val, _ = torch.max(max_val, 4)
	local exp_map = torch.exp(self.beta * (input - max_val:expandAs(input)))
	local sum_vec = torch.sum(torch.sum(exp_map, 3), 4)
	local prob_map = torch.cdiv(exp_map, sum_vec:expandAs(exp_map))
	local arg_x = torch.sum(torch.sum(torch.cmul(x_range, prob_map), 3), 4)
	local arg_y = torch.sum(torch.sum(torch.cmul(y_range, prob_map), 3), 4)
	self.output[{{}, {1}}] = arg_x
	self.output[{{}, {2}}] = arg_y
	return self.output
end

function SoftArgMax:updateGradInput(input, gradOutput)
	self.gradInput:resizeAs(input):fill(0)
	return self.gradInput
end

function SoftArgMax:reset()
end





