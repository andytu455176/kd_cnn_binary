local GHHPooling, parent = torch.class('nn.GHHPooling', 'nn.Module')

function GHHPooling:__init(num_output_, num_in_sum_, num_in_max_)
	parent.__init(self)
	self.ns = num_in_sum_
	self.nm = num_in_max_
	self.nu = num_output_
	self.max_indices = torch.LongTensor()
end

function GHHPooling:check(input, gradOutput)
	assert(input:size(2) == self.ns * self.nm * self.nu)
end

function GHHPooling:updateOutput(input)
	assert(input.THNN, torch.type(input)..'.THNN backend not imported')
	self:check(input)

	-- input.THNN.GHHPooling_updateOutput(
	-- 	input:cdata(), 
	-- 	input:cdata()
	-- )

	local axis = 2
	local output_shape = input:size()
	output_shape[axis] = self.nu
	self.output:resize(output_shape):fill(0)
	output_shape[axis] = self.nu*self.ns 
	self.max_indices:resize(output_shape):fill(0)
	for i=1,self.nu do 
		for j=1,self.ns do 
			local cur_max, cur_indices 
			local offset = (i-1)*self.nm*self.ns + (j-1)*self.nm + 1
			local delta = 1.0 - 2.0 * ((j-1)%2)
			cur_max, cur_indices = torch.max(input[{{}, {offset, offset + self.nm-1}}], axis)
			self.max_indices:select(2, (i-1)*self.ns+j):copy(cur_indices)
			self.output[{{}, {i}}] = self.output[{{}, {i}}] + cur_max * delta 
		end
	end

	return self.output
end

function GHHPooling:updateGradInput(input, gradOutput)
	
	local axis = 2 
	self.gradInput:resizeAs(input):fill(0)

	for i=1,self.nu do 
		for j = 1,self.ns do 
			local cur_indices = self.max_indices[{{}, {(i-1)*self.ns+j}}]
			local offset = (i-1)*self.nm*self.ns + (j-1)*self.nm + 1
			local delta = 1.0 - 2.0 * ((j-1)%2)
			self.gradInput[{{}, {offset, offset + self.nm-1}}]:scatter(axis, cur_indices, gradOutput[{{}, {i}}] * delta)
		end
	end

	return self.gradInput
end

function GHHPooling:reset()
end



