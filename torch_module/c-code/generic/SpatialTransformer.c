#ifndef TH_GENERIC_FILE 
#define TH_GENERIC_FILE "generic/SpatialTransformer.c"
#else

void THNN_(SpatialTransformer_updateOutput) (
		THNNState *state, 
		THTensor *input, 
		THTensor *output, 
		THTensor *param, 
		THTensor *patch_mask, 
		THTensor *transformed_mask, 
		THTensor *coor_range, 
		int output_H, int output_W) 
{
	bool debug = false;
	char string[128] = "\t\t SpatialTransformer:: UpdateOutput:\t";
	int N_ = input->size[0];
	int C_ = input->size[1];
	int H_ = input->size[2];
	int W_ = input->size[3];

	THTensor_(resize4d)(output, N_, C_, output_H, output_W);
	THTensor_(fill)(output, 0.0);
	input = THTensor_(newContiguous)(input); // NEED TO FREE 
	output = THTensor_(newContiguous)(output); // NEED TO FREE 
	real *input_data = THTensor_(data)(input); // newContiguous 
	real *output_data = THTensor_(data)(output); // just resized -> newContiguous
	THTensor_(resize3d)(transformed_mask, N_, output_H*output_W, 2);
	THTensor_(resize4d)(coor_range, N_, output_H, output_W, 4);
		
	// get transformed mask 
	THTensor_(baddbmm)(transformed_mask, 0.0, transformed_mask, 1.0, patch_mask, param);

	transformed_mask = THTensor_(newContiguous)(transformed_mask); // NEED TO FREE
	coor_range = THTensor_(newContiguous)(coor_range); // NEED TO FREE 
	real *transformed_mask_data = THTensor_(data)(transformed_mask); // newContiguous 
	real *coor_range_data = THTensor_(data)(coor_range); // just resized -> newContiguous

	int n, c, h, w;
	for (n = 0; n < N_; n++) {
		for (h = 0; h < output_H; h++) {
			for (w = 0; w < output_W; w++) {
				long coor_offset = n*transformed_mask->stride[0] + (h*output_W + w)*transformed_mask->stride[1];
				real x = transformed_mask_data[coor_offset];
				real y = transformed_mask_data[coor_offset + transformed_mask->stride[2]];

				int w_min = (floor(y) > 0) ? floor(y) : 0;
                int w_max = (ceil(y) < W_ - 1) ? ceil(y) : (W_ - 1);
                int h_min = (floor(x) > 0) ? floor(x) : 0;
                int h_max = (ceil(x) < H_ - 1) ? ceil(x) : (H_ - 1);

				long coor_range_offset = n*coor_range->stride[0] + h*coor_range->stride[1] + w*coor_range->stride[2];
				coor_range_data[coor_range_offset + 0] = w_min;
				coor_range_data[coor_range_offset + 1] = w_max;
				coor_range_data[coor_range_offset + 2] = h_min;
				coor_range_data[coor_range_offset + 3] = h_max;

				int hh, ww;
				for (hh = h_min; hh <= h_max; hh++) {
					for (ww = w_min; ww <= w_max; ww++) {
						for (c = 0; c < C_; c++) {
							real weight = (1 - fabs(x - hh)) * (1 - fabs(y - ww));
							long input_offset = n*input->stride[0] + c*input->stride[1] + 
												hh*input->stride[2] + ww*input->stride[3];
							long output_offset = n*output->stride[0] + c*output->stride[1] + 
												 h*output->stride[2] + w*output->stride[3];
							output_data[output_offset] += weight * input_data[input_offset];
						}
					}
				}
			}
		}
	}
	THTensor_(free)(input);
	THTensor_(free)(transformed_mask);
	THTensor_(free)(output);
	THTensor_(free)(coor_range);
	if (debug) printf("%s Finish!\n", string);
}

void THNN_(SpatialTransformer_updateGradInput) (
		THNNState *state, 
		THTensor *input, 
		THTensor *output_diff, 
		THTensor *input_diff, 
		THTensor *param_diff, 
		THTensor *transformed_mask, 
		THTensor *coor_range, 
		THTensor *patch_mask, 
		int output_H, int output_W) 
{	
	bool debug = false;
	char string[128] = "\t\t SpatialTransformer:: UpdateGradInput:\t";
	int N_ = input->size[0];
	int C_ = input->size[1];
	int H_ = input->size[2];
	int W_ = input->size[3];

	THTensor_(resizeAs)(input_diff, input);
	THTensor_(resize3d)(param_diff, N_, 3, 2);
	THTensor_(fill)(input_diff, 0.0);
	THTensor_(fill)(param_diff, 0.0);

	output_diff = THTensor_(newContiguous)(output_diff); // NEED TO FREE 
	coor_range = THTensor_(newContiguous)(coor_range); // NEED TO FREE 
	transformed_mask = THTensor_(newContiguous)(transformed_mask); // NEED TO FREE 
	real *output_diff_data = THTensor_(data)(output_diff); // newContiguous 
	real *coor_range_data = THTensor_(data)(coor_range); // newContiguous 
	real *transformed_mask_data = THTensor_(data)(transformed_mask); // newContiguous

	input = THTensor_(newContiguous)(input); // NEED TO FREE 
	patch_mask = THTensor_(newContiguous)(patch_mask); // NEED TO FREE 
	input_diff = THTensor_(newContiguous)(input_diff); // NEED TO FREE 
	param_diff = THTensor_(newContiguous)(param_diff); // NEED TO FREE 
	real *input_diff_data = THTensor_(data)(input_diff); // just resized -> newContiguous
	real *input_data = THTensor_(data)(input); // newContiguous
	real *param_diff_data = THTensor_(data)(param_diff); // just resized -> newContiguous
	real *patch_mask_data = THTensor_(data)(patch_mask); // newContiguous

	int n, c, h, w;
	for (n = 0; n < N_; n++) {
		for (h = 0; h < output_H; h++) {
			for (w = 0; w < output_W; w++) {
				long coor_offset = n*transformed_mask->stride[0] + (h*output_W + w)*transformed_mask->stride[1];
				real x = transformed_mask_data[coor_offset];
				real y = transformed_mask_data[coor_offset + transformed_mask->stride[2]];

				long coor_range_offset = n*coor_range->stride[0] + h*coor_range->stride[1] + w*coor_range->stride[2];
				int w_min = coor_range_data[coor_range_offset + 0];
				int w_max = coor_range_data[coor_range_offset + 1];
				int h_min = coor_range_data[coor_range_offset + 2];
				int h_max = coor_range_data[coor_range_offset + 3];

				real tmp_source_x = 0;
				real tmp_source_y = 0;

				int hh, ww; 
				for (hh = h_min; hh <= h_max; hh++) {
					for (ww = w_min; ww <= w_max; ww++) {
						int sign_x = ((hh - x) > 0) - ((hh - x) < 0);
						int sign_y = ((ww - y) > 0) - ((ww - y) < 0);

						for (c = 0; c < C_; c++) {
							long dv_offset = n*output_diff->stride[0] + c*output_diff->stride[1] + 
											 h*output_diff->stride[2] + w*output_diff->stride[3];
							long du_offset = n*input_diff->stride[0] + c*input_diff->stride[1] + 
                                             hh*input_diff->stride[2] + ww*input_diff->stride[3];
							real dv = output_diff_data[dv_offset];
							// dU
							input_diff_data[du_offset] += dv * (1 - fabs(x - hh)) * (1 - fabs(y - ww));

							dv = dv * input_data[du_offset];
							tmp_source_x += dv * (1 - fabs(y - ww)) * sign_x;
							tmp_source_y += dv * (1 - fabs(x - hh)) * sign_y;
						}
					}
				}
				long dtheta_offset = n*param_diff->stride[0];
				long dtheta_s1 = param_diff->stride[1];
				long dtheta_s2 = param_diff->stride[2];
				long pm_offset_x = n*patch_mask->stride[0] + (h*output_W + w)*patch_mask->stride[1];
				long pm_offset_y = n*patch_mask->stride[0] + (h*output_W + w)*patch_mask->stride[1] + patch_mask->stride[2];
				// dTheta 
				param_diff_data[dtheta_offset + 0*dtheta_s1+0*dtheta_s2] += tmp_source_x * patch_mask_data[pm_offset_x];
				param_diff_data[dtheta_offset + 1*dtheta_s1+0*dtheta_s2] += tmp_source_x * patch_mask_data[pm_offset_y];
				param_diff_data[dtheta_offset + 2*dtheta_s1+0*dtheta_s2] += tmp_source_x * 1.0;
				param_diff_data[dtheta_offset + 0*dtheta_s1+1*dtheta_s2] += tmp_source_y * patch_mask_data[pm_offset_x];
				param_diff_data[dtheta_offset + 1*dtheta_s1+1*dtheta_s2] += tmp_source_y * patch_mask_data[pm_offset_y];
				param_diff_data[dtheta_offset + 2*dtheta_s1+1*dtheta_s2] += tmp_source_y * 1.0;
			}
		}
	}

	THTensor_(free)(output_diff);
	THTensor_(free)(coor_range);
	THTensor_(free)(transformed_mask);
	THTensor_(free)(input);
	THTensor_(free)(patch_mask);
	THTensor_(free)(input_diff);
	THTensor_(free)(param_diff);
	if (debug) printf("%s Finish!\n", string);
}




#endif 
