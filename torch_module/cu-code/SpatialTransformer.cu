#include "THCUNN.h"
#include "common.h"

#include "THCHalf.h"
#include "THCHalfAutoNumerics.cuh"
#include "THCAtomics.cuh"


template <typename Dtype>
__global__ void SpatialTransformerForward(const int nthreads, const int H_, const int W_, const int output_H, const int output_W, const int C_, const Dtype *input_data, const Dtype *transformed_mask_data, Dtype *coor_range_data, Dtype *output_data)
{
	CUDA_KERNEL_LOOP(index, nthreads) {
		// N_ * C_ * output_H, output_W 
		const int c_map_size = C_ * output_H * output_W;
		const int map_size = output_H * output_W;
		const int c_rem = index % c_map_size;
		const int rem = c_rem % map_size;

		const int n = index / c_map_size;
		const int c = c_rem / map_size;
		const int h = rem / output_W;
		const int w = rem % output_W;

		const int coor_offset = n * (map_size*2) + (h*output_W+w) * 2;
		Dtype x = transformed_mask_data[coor_offset];
		Dtype y = transformed_mask_data[coor_offset + 1];
		
		float x_f = ScalarConvert<Dtype, float>::to(x);
		float y_f = ScalarConvert<Dtype, float>::to(y);
		int w_min = (floor(y_f) > 0) ? floor(y_f) : 0;
		int w_max = (ceil(y_f) < W_ - 1) ? ceil(y_f) : (W_ - 1);
		int h_min = (floor(x_f) > 0) ? floor(x_f) : 0;
		int h_max = (ceil(x_f) < H_ - 1) ? ceil(x_f) : (H_ - 1);

		const int coor_range_offset = n * (map_size*4) + h * (output_W*4) + w * 4;
		coor_range_data[coor_range_offset + 0] = ScalarConvert<int, Dtype>::to(w_min);
		coor_range_data[coor_range_offset + 1] = ScalarConvert<int, Dtype>::to(w_max);
		coor_range_data[coor_range_offset + 2] = ScalarConvert<int, Dtype>::to(h_min);
		coor_range_data[coor_range_offset + 3] = ScalarConvert<int, Dtype>::to(h_max);

		int hh, ww;
		Dtype res = ScalarConvert<float, Dtype>::to(0.0);
		const int input_offset = n * (C_*H_*W_) + c * (H_*W_);
		const int output_offset = n * c_map_size + c * map_size;
		for (hh = h_min; hh <= h_max; hh++) {
			for (ww = w_min; ww <= w_max; ww++) {
				float x_dist = ScalarConvert<Dtype, float>::to(x - hh);
				float y_dist = ScalarConvert<Dtype, float>::to(y - ww);
				Dtype weight = ScalarConvert<float, Dtype>::to((1 - fabs(x_dist)) * (1 - fabs(y_dist)));
				res += input_data[input_offset + hh*W_ + ww] * weight;
			}
		}
		output_data[output_offset + h * output_W + w] = res;
	}
}


template <typename Dtype>
__global__ void SpatialTransformerBackward(const int nthreads, const int H_, const int W_, const int output_H, const int output_W, const int C_, const Dtype *input_data, const Dtype *output_diff_data, const Dtype *transformed_mask_data, const Dtype *coor_range_data, const Dtype *patch_mask_data, Dtype *input_diff_data, Dtype *param_diff_data)
{
	CUDA_KERNEL_LOOP(index, nthreads) {
		// N_ * C_ * output_H, output_W 
		const int c_map_size = C_ * output_H * output_W;
		const int map_size = output_H * output_W;
		const int c_rem = index % c_map_size;
		const int rem = c_rem % map_size;

		const int n = index / c_map_size;
		const int c = c_rem / map_size;
		const int h = rem / output_W;
		const int w = rem % output_W;

		const int coor_offset = n * (map_size*2) + (h*output_W+w) * 2;
		Dtype x = transformed_mask_data[coor_offset];
		Dtype y = transformed_mask_data[coor_offset + 1];

		const int coor_range_offset = n * (map_size*4) + h * (output_W*4) + w * 4;
		int w_min = ScalarConvert<Dtype, int>::to(coor_range_data[coor_range_offset + 0]);
		int w_max = ScalarConvert<Dtype, int>::to(coor_range_data[coor_range_offset + 1]);
		int h_min = ScalarConvert<Dtype, int>::to(coor_range_data[coor_range_offset + 2]);
		int h_max = ScalarConvert<Dtype, int>::to(coor_range_data[coor_range_offset + 3]);

		Dtype tmp_source_x = ScalarConvert<float, Dtype>::to(0.0);
		Dtype tmp_source_y = ScalarConvert<float, Dtype>::to(0.0);

		int hh, ww; 
		const int du_offset = n * (C_*H_*W_) + c * (H_*W_);
		const int dv_offset = n * c_map_size + c * map_size;
		Dtype dv = output_diff_data[dv_offset + h * output_W + w];
		for (hh = h_min; hh <= h_max; hh++) {
			for (ww = w_min; ww <= w_max; ww++) {
				Dtype sign_x = ScalarConvert<int, Dtype>::to(((hh - x) > 0) - ((hh - x) < 0));
				Dtype sign_y = ScalarConvert<int, Dtype>::to(((ww - y) > 0) - ((ww - y) < 0));

				// dU 
				float x_dist = ScalarConvert<Dtype, float>::to(x - hh);
				float y_dist = ScalarConvert<Dtype, float>::to(y - ww);
				Dtype inc = ScalarConvert<float, Dtype>::to(dv * (1 - fabs(x_dist)) * (1 - fabs(y_dist)));
				atomicAdd(&input_diff_data[du_offset + hh * W_ + ww], inc);
				Dtype Udv = dv * input_data[du_offset + hh * W_ + ww];
				tmp_source_x += ScalarConvert<float, Dtype>::to(Udv * (1 - fabs(y_dist)) * sign_x);
				tmp_source_y += ScalarConvert<float, Dtype>::to(Udv * (1 - fabs(x_dist)) * sign_y);
			}
		}

		const int dtheta_offset = n * 6; 
		const int dtheta_s1 = 2;
		const int dtheta_s2 = 1;
		const int pm_offset_x = n * (3*map_size) + (h*output_W + w) * 3;
		const int pm_offset_y = n * (3*map_size) + (h*output_W + w) * 3 + 1;

		// dTheta 
		atomicAdd(&param_diff_data[dtheta_offset + 0*dtheta_s1+0*dtheta_s2], tmp_source_x * patch_mask_data[pm_offset_x]);
		atomicAdd(&param_diff_data[dtheta_offset + 1*dtheta_s1+0*dtheta_s2], tmp_source_x * patch_mask_data[pm_offset_y]);
		atomicAdd(&param_diff_data[dtheta_offset + 2*dtheta_s1+0*dtheta_s2], 
				tmp_source_x * ScalarConvert<float, Dtype>::to(1.0));

		atomicAdd(&param_diff_data[dtheta_offset + 0*dtheta_s1+1*dtheta_s2], tmp_source_y * patch_mask_data[pm_offset_x]);
		atomicAdd(&param_diff_data[dtheta_offset + 1*dtheta_s1+1*dtheta_s2], tmp_source_y * patch_mask_data[pm_offset_y]);
		atomicAdd(&param_diff_data[dtheta_offset + 2*dtheta_s1+1*dtheta_s2], 
				tmp_source_y * ScalarConvert<float, Dtype>::to(1.0));
	}
}




#include "generic/SpatialTransformer.cu"
#include "THCGenerateFloatTypes.h"
