#ifndef THC_GENERIC_FILE 
#define THC_GENERIC_FILE "generic/SpatialTransformer.cu"
#else 

void THNN_(SpatialTransformer_updateOutput) (
		THCState *state, 
		THCTensor *input, 
		THCTensor *output, 
		THCTensor *param, 
		THCTensor *patch_mask, 
		THCTensor *transformed_mask, 
		THCTensor *coor_range, 
		int output_H, int output_W) 
{
	bool debug = false;
	char string[128] = "\t\t SpatialTransformer:: UpdateOutput:\t";
	int N_ = input->size[0];
	int C_ = input->size[1];
	int H_ = input->size[2];
	int W_ = input->size[3];

	THCTensor_(resize4d)(state, output, N_, C_, output_H, output_W);
	THCTensor_(fill)(state, output, ScalarConvert<float, real>::to(0.0));

	input = THCTensor_(newContiguous)(state, input); // NEED TO FREE 
	real *input_data = THCTensor_(data)(state, input); // newContiguous
	real *output_data = THCTensor_(data)(state, output); // just resized 
	THCTensor_(resize3d)(state, transformed_mask, N_, output_H*output_W, 2);
	THCTensor_(resize4d)(state, coor_range, N_, output_H, output_W, 4);

	// get transformed mask 
	THCTensor_(baddbmm)(state, transformed_mask, ScalarConvert<float, real>::to(0.0), 
			transformed_mask, ScalarConvert<float, real>::to(1.0), patch_mask, param);

	transformed_mask = THCTensor_(newContiguous)(state, transformed_mask); // NEED TO FREE 
	real *transformed_mask_data = THCTensor_(data)(state, transformed_mask); // newContiguous
	real *coor_range_data = THCTensor_(data)(state, coor_range); // just resized 


	int count = THCTensor_(nElement)(state, output);
	SpatialTransformerForward<real>
		<<<GET_BLOCKS(count), CUDA_NUM_THREADS, 0, THCState_getCurrentStream(state) >>>(
				count, H_, W_, output_H, output_W, C_, input_data, transformed_mask_data, coor_range_data, output_data);
	
	THCudaCheck(cudaGetLastError());

	THCTensor_(free)(state, input);
	THCTensor_(free)(state, transformed_mask);
	
	if (debug) printf("%s Finish!\n", string);
}


void THNN_(SpatialTransformer_updateGradInput) (
		THCState *state, 
		THCTensor *input, 
		THCTensor *output_diff, 
		THCTensor *input_diff, 
		THCTensor *param_diff, 
		THCTensor *transformed_mask, 
		THCTensor *coor_range, 
		THCTensor *patch_mask, 
		int output_H, int output_W) 
{	
	bool debug = false;
	char string[128] = "\t\t SpatialTransformer:: UpdateGradInput:\t";
	int N_ = input->size[0];
	int C_ = input->size[1];
	int H_ = input->size[2];
	int W_ = input->size[3];

	THCTensor_(resizeAs)(state, input_diff, input);
	THCTensor_(resize3d)(state, param_diff, N_, 3, 2);
	THCTensor_(fill)(state, input_diff, ScalarConvert<float, real>::to(0.0));
	THCTensor_(fill)(state, param_diff, ScalarConvert<float, real>::to(0.0));

	output_diff = THCTensor_(newContiguous)(state, output_diff); // NEED TO FREE 
	coor_range = THCTensor_(newContiguous)(state, coor_range); // NEED TO FREE 
	transformed_mask = THCTensor_(newContiguous)(state, transformed_mask); // NEED TO FREE 
	real *output_diff_data = THCTensor_(data)(state, output_diff); // newContiguous 
	real *coor_range_data = THCTensor_(data)(state, coor_range); // newContiguous 
	real *transformed_mask_data = THCTensor_(data)(state, transformed_mask); // newContiguous

	input = THCTensor_(newContiguous)(state, input); // NEED TO FREE 
	patch_mask = THCTensor_(newContiguous)(state, patch_mask); // NEED TO FREE 
	real *input_diff_data = THCTensor_(data)(state, input_diff); // just resized 
	real *input_data = THCTensor_(data)(state, input); // newContiguous
	real *param_diff_data = THCTensor_(data)(state, param_diff); // just resized 
	real *patch_mask_data = THCTensor_(data)(state, patch_mask); // newContiguous 


	int count = THCTensor_(nElement)(state, output_diff);
	SpatialTransformerBackward<real>
		<<<GET_BLOCKS(count), CUDA_NUM_THREADS, 0, THCState_getCurrentStream(state) >>>(
				count, H_, W_, output_H, output_W, C_, 
				input_data, output_diff_data, transformed_mask_data, coor_range_data, patch_mask_data, 
				input_diff_data, param_diff_data);

	THCudaCheck(cudaGetLastError());

	THCTensor_(free)(state, output_diff);
	THCTensor_(free)(state, coor_range);
	THCTensor_(free)(state, transformed_mask);
	THCTensor_(free)(state, input);
	THCTensor_(free)(state, patch_mask);

	if (debug) printf("%s Finish!\n", string);
}




#endif 
