import sys, os, re
import numpy as np 
import cv2
import struct, cPickle
import matplotlib.pyplot as plt 
from collections import defaultdict 
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description="Extract patches arguments")
    parser.add_argument('--target', dest='target', help='keypoint patches (kp) or non-keypoint patches (non-kp)', 
            required=True, type=str)
    parser.add_argument('--filename', dest='filename', help='target nvm file', required=True, type=str)
    parser.add_argument('--output', dest='output', help='output directory', required=True, type=str)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args 

def init_output_dir_struct(output_dir_name):
    mkdir_list = [output_dir_name, os.path.join(output_dir_name, 'info'), 
                  os.path.join(output_dir_name, 'pos_patches'), 
                  os.path.join(output_dir_name, 'neg_patches')]

    for path_name in mkdir_list:
        if not os.path.exists(path_name):
            os.mkdir(path_name)
    return 

def parse_nvm_camera(filename, output_dir_name, subdir):
    image_dir_name = os.path.dirname(filename)
    lines = open(filename, 'r').readlines()
    num_camera = int(lines[2][:-1])
    cameras = []
    loc_data_dict = {}
    image_list = [] # (image, image_id)
 
    for i in range(num_camera):
        camera_name = os.path.abspath(os.path.join(image_dir_name, re.split('\t', lines[3+i][:-1])[0]))
        sift_filename = os.path.splitext(camera_name)[0] + '.sift'
        cameras.append(camera_name)
        loc_data_dict[camera_name] = read_sift_file(sift_filename)
        #image_list.append((cv2.imread(camera_name, cv2.IMREAD_GRAYSCALE), 
        #                   os.path.splitext(os.path.basename(camera_name))[0]))
        image_list.append((None, 
                           os.path.splitext(os.path.basename(camera_name))[0]))

        if not os.path.exists(os.path.join(output_dir_name, subdir, image_list[i][1])):
            os.mkdir(os.path.join(output_dir_name, subdir, image_list[i][1]))

    return image_dir_name, num_camera, cameras, loc_data_dict, image_list, lines 

def gen_keypoint_patches(filename, output_dir_name):

    """
        patch_name: %s/%06d_%05d.png (image_name, 3dpt_index, feat_index)
    """

    num_observation = 5
    init_output_dir_struct(output_dir_name)

    image_dir_name, num_camera, cameras, loc_data_dict, image_list, lines = parse_nvm_camera(filename, 
                                                                                    output_dir_name, 'pos_patches')

    num_3d_pt = int(lines[4+num_camera])
    offset = 5+num_camera

    camera2pt = defaultdict(list)
    for i in range(num_3d_pt): 
        sys.stdout.flush()
        sys.stdout.write("prog: %d/%d          \r" % (i, num_3d_pt))
        split_line = filter(None, lines[offset + i][:-1].split(' '))

        pt_3d = [float(x) for x in split_line[:3]]
        pt_rgb = [float(x) for x in split_line[3:6]]
        num_measure = int(split_line[6])
        if num_measure < num_observation:
            continue 
        for count, j in enumerate(range(7, len(split_line), 4)):
            image_index = int(split_line[j])
            feat_index = int(split_line[j+1])
            x, y = [float(split_line[j+a]) for a in range(2, 4)]
            #image = image_list[image_index][0]
            image = cv2.imread(cameras[image_index], cv2.IMREAD_GRAYSCALE)
            middle = (image.shape[1]//2, image.shape[0]//2)

            # record the point 
            camera2pt[cameras[image_index]].append((middle[0]+x, middle[1]+y))

            # get the positive patches 
            scale = loc_data_dict[cameras[image_index]][feat_index][3]
            scale_size = int(24 * scale)
            perturbation = np.random.rand(2) * 4.8 * scale 
            standard_size = 128 
            patch_name = os.path.join(output_dir_name, "pos_patches/%s/%06d_%05d.png" % (image_list[image_index][1], i, feat_index))
            #print scale_size, perturbation, patch_name
            new_center = (np.array([middle[0]+x, middle[1]+y], dtype=np.float32) + perturbation).astype(np.int32)
            x_range = (int(new_center[0] - scale_size/2.0), int(new_center[0] + scale_size/2.0))
            y_range = (int(new_center[1] - scale_size/2.0), int(new_center[1] + scale_size/2.0))
            pad_x_range = (max(0, 0-x_range[0]), max(0, x_range[1]-image.shape[1]))
            pad_y_range = (max(0, 0-y_range[0]), max(0, y_range[1]-image.shape[0]))
            cropped_patch = np.lib.pad(image[max(y_range[0], 0):y_range[1], max(x_range[0], 0):x_range[1]], 
                                        ((pad_y_range[0], pad_y_range[1]), (pad_x_range[0], pad_x_range[1])), 
                                        'constant', constant_values=(0, 0))
            patch = cv2.resize(cropped_patch, (standard_size, standard_size))
            cv2.imwrite(patch_name, patch)
            
    cPickle.dump(camera2pt, open(os.path.join(output_dir_name, 'info/camera2pt.pkl'), 'w'))
    return 

def gen_non_keypoint_patches(filename, output_dir_name):

    init_output_dir_struct(output_dir_name)
    image_dir_name, num_camera, cameras, loc_data_dict, image_list, _ = parse_nvm_camera(filename, 
                                                                                output_dir_name, 'neg_patches')

    for i in range(num_camera):
        camera_name = cameras[i]
        loc_data = loc_data_dict[camera_name]
        excluded_pts = loc_data[:, :2]
        min_scale = np.min(loc_data[:, 3])
        image = cv2.imread(camera_name, cv2.IMREAD_GRAYSCALE)
        image_id = image_list[i][1]

        # generate excluded-point patches 
        max_neg_num = 2000 
        image_shape = np.array(image.shape[::-1], dtype=np.float32)
        max_fail_count = 5
        tol_dist = 24.0 * min_scale * 0.75
        index_count = len(os.listdir(os.path.join(output_dir_name, 'neg_patches', image_id)))
        for j in range(max_neg_num):
            fail_count = 0
            sys.stdout.flush()
            sys.stdout.write("prog: %d/%d, %d/%d %d,    min_scale: %f     \r" % (i, num_camera, j, max_neg_num, fail_count, min_scale))
            while fail_count < max_fail_count:
                new_center = np.random.rand(2) * image_shape
                min_dist = np.min(np.sum((excluded_pts - new_center)**2)) - 25.0
                if min_dist >= tol_dist:
                    patch_size = int(np.sqrt(np.random.rand() * (min_dist - tol_dist) + tol_dist)) * 2.0
                    x_range = (int(new_center[0] - patch_size/2.0), int(new_center[0] + patch_size/2.0))
                    y_range = (int(new_center[1] - patch_size/2.0), int(new_center[1] + patch_size/2.0))
                    pad_x_range = (max(0, 0-x_range[0]), max(0, x_range[1]-image.shape[1]))
                    pad_y_range = (max(0, 0-y_range[0]), max(0, y_range[1]-image.shape[0]))
                    cropped_patch = np.lib.pad(image[max(y_range[0], 0):y_range[1], max(x_range[0], 0):x_range[1]], 
                                                ((pad_y_range[0], pad_y_range[1]), (pad_x_range[0], pad_x_range[1])), 
                                                'constant', constant_values=(0, 0))
                    patch = cv2.resize(cropped_patch, (standard_size, standard_size))
                    patch_name = os.path.join(output_dir_name, 'neg_patches/%s/%5d.png' % (image_id, index_count))
                    cv2.imwrite(patch_name, patch)
                    index_count = index_count + 1
                    break
                fail_count = fail_count + 1
    return 

def read_sift_file(filename):
    f = open(filename, 'rb')
    version_type = f.read(8)
    num_kp = struct.unpack('i', f.read(4))
    dim1 = struct.unpack('i', f.read(4))
    dim2 = struct.unpack('i', f.read(4))
    print "filename: ", os.path.basename(filename), ", info: ", num_kp, dim1, dim2
    loc_data_list = []

    #### for reading the loc data 
    for i in range(num_kp[0]):
        loc_data = [struct.unpack('f', f.read(4))[0] for x in range(dim1[0])]
        loc_data_list.append(loc_data)

    ##### for reading the desc data 
    #for i in range(num_kp[0]):
    #    desc_data = [struct.unpack('B', f.read(1))[0] for x in range(dim2[0])]
    #    print desc_data

    return loc_data_list

def show_sift_kp(image_name):
    image = cv2.imread(image_name, cv2.IMREAD_GRAYSCALE)
    loc_data = read_sift_file(os.path.splitext(image_name)[0]+'.sift')
    plt.imshow(image, cmap='Greys_r')
    for point in loc_data:
        plt.plot(point[0], point[1], 'o', ms=5)
    plt.show()
    return 

if __name__ == "__main__":
    args = parse_args()

    if args.target == 'kp':
        gen_keypoint_patches(args.filename, args.output)
    elif args.target == 'non-kp':
        gen_non_keypoint_patches(args.filename, args.output)
    else:
        print '\033[91m' + 'Target "' + args.target + '" is not supported' + '\033[0m'
        exit(-1)












