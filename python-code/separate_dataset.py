import numpy as np 
from random import shuffle 
import os, sys
import shutil 

def separate_train_test_set(target_dir):
    dir_list = os.listdir(os.path.join(target_dir, 'pos_patches'))
    shuffle(dir_list)
    num_train_image = np.round(len(dir_list) * 5.0/7.0).astype(np.int32)
    num_test_image = np.round(len(dir_list) * 2.0/7.0).astype(np.int32)
    train_image_list = dir_list[:num_train_image]
    test_image_list = dir_list[num_train_image:]
    
    print("num_train_image: ", len(train_image_list), "num_test_image: ", len(test_image_list))
    
    # For TrainData 
    if not os.path.exists(os.path.join(target_dir, 'TrainData/pos_patches')):
        if not os.path.exists(os.path.join(target_dir, 'TrainData')):
            os.mkdir(os.path.join(target_dir, 'TrainData'))
        os.mkdir(os.path.join(target_dir, 'TrainData/pos_patches'))
    
    for image_dir in train_image_list:
        shutil.move(os.path.join(target_dir, 'pos_patches', image_dir), 
                os.path.join(target_dir, 'TrainData/pos_patches'))
    
    # For TestData 
    if not os.path.exists(os.path.join(target_dir, 'TestData/pos_patches')):
        if not os.path.exists(os.path.join(target_dir, 'TestData')):
            os.mkdir(os.path.join(target_dir, 'TestData'))
        os.mkdir(os.path.join(target_dir, 'TestData/pos_patches'))
    
    for image_dir in test_image_list:
        shutil.move(os.path.join(target_dir, 'pos_patches', image_dir), 
                os.path.join(target_dir, 'TestData/pos_patches'))

    return 


if __name__ == "__main__":
    dir_name = sys.argv[1]
    if os.path.exists(dir_name) and os.path.isdir(dir_name):
        separate_train_test_set(dir_name)
    else:
        print '\033[91m' + 'Target dir_name "' + dir_name + '" is not a directory' + '\033[0m'
        exit(-1)


