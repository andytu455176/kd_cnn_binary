import os, sys, cv2
import numpy as np 
import scipy.io
import itertools 
from collections import defaultdict 
import argparse 

def parse_args():
    parser = argparse.ArgumentParser(description="Preprocessing for train test data arguments")

    parser.add_argument('--action', dest='action', 
            help='gen_image_list (genImgList) or gen_pair_list (genPairs) or calculate_mean_std (calcMeanStd)', 
            required=True, type=str)

    parser.add_argument('--list_dir', dest='list_dir', help='the dir name of the output list or info', 
            required=True, type=str)

    parser.add_argument('--input_dir', dest='input_dir', help='the input dir for "genImgList" action', default=None, type=str)
    parser.add_argument('--input_file', dest='input_file', help='the input file for "genPairs" and "calcMeanStd" action ', 
            default=None, type=str)

    parser.add_argument('--output', dest='output', help='the output file for the action', required=True, type=str)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def gen_image_list(target_dir, output_name):
    f = open(output_name, 'w')
    target_dir_list = os.listdir(target_dir)
    for i, sub_dir in enumerate(target_dir_list):
        sub_dir_list = os.listdir(os.path.join(target_dir, sub_dir))
        for j, filename in enumerate(sub_dir_list):
            sys.stdout.flush()
            sys.stdout.write('prog: %d/%d %d/%d                    \r' % (i, len(target_dir_list), j, len(sub_dir_list)))
            f.write('%s\n' % os.path.join(os.path.abspath(target_dir), sub_dir, filename))
    f.close()
    return 

def gen_pair_list(image_list_file, output_name):
    print "load_image_list_structure() ..."
    _, pt3d_id_table, num_image = load_image_list_structure(image_list_file)
    pair_list = []
    label_list = []

    print "num_image: ", num_image
    print "sample pairs ..."
    for pt3d_id, image_list in enumerate(pt3d_id_table):
        sys.stdout.flush()
        sys.stdout.write("prog: %d/%d                      \r" % (pt3d_id, len(pt3d_id_table)))

        # pos_pairs 
        sampled_pairs, labels = sample_pos_patches(image_list)
        pair_list.extend(sampled_pairs)
        label_list.extend(labels)

        # neg_pairs 
        num_neg = 2
        for index1, image_id1 in enumerate(image_list):
            for i in range(num_neg):
                if pt3d_id < len(pt3d_id_table) - 1:
                    other_pt3d_id = np.random.randint(pt3d_id+1, len(pt3d_id_table))
                else:
                    other_pt3d_id = np.random.randint(0, len(pt3d_id_table)-1)
                index2 = np.random.randint(0, len(pt3d_id_table[other_pt3d_id]))
                image_id2 = pt3d_id_table[other_pt3d_id][index2]
                pair_list.append((image_id1, image_id2))
                label_list.append(-1)
        
    pairs = np.array(pair_list, dtype=np.int32)
    labels = np.array(label_list, dtype=np.float32)
    
    print "num pairs: ", pairs.shape[0]

    temp = {}
    temp['pairs'] = pairs + 1
    temp['labels'] = labels
    scipy.io.savemat(output_name, temp)
    return 

def sample_pos_patches(image_list):
    sampled_pairs = []
    labels = []
    first_index_table = defaultdict(list)
    choose_set = set()

    if len(image_list) == 2:
        sampled_pairs = [(image_list[0], image_list[1])]
        labels = [1]
    elif len(image_list) > 2:
        for p in itertools.combinations(range(len(image_list)), 2):
            first_index_table[p[0]].append(p)
            choose_set.add(p)
        for i in range(len(image_list)):
            # TODO: maybe choose from front and then choose from back 
            if len(first_index_table[i]) > 0:
                chosen_pair = first_index_table[i][np.random.randint(0, len(first_index_table[i]))]
            else:
                choose_list = list(choose_set)
                chosen_pair = choose_list[np.random.randint(0, len(choose_list))]
            sampled_pairs.append((image_list[chosen_pair[0]], image_list[chosen_pair[1]]))
            labels.append(1)
            choose_set.remove(chosen_pair)
            
    return sampled_pairs, labels 

def load_image_list(input_name):
    image_table = []
    lines = open(input_name, 'r').readlines()
    for line in lines:
        image_table.append(line)
    return image_table 

def load_image_list_structure(input_name):
    image_table = []

    pt3d_id_table = []
    index_map = {}

    lines = open(input_name, 'r').readlines()
    for count, line in enumerate(lines): 
        # image table 
        image_table.append(line)

        # pt3d_id_table 
        pt3d_id = os.path.basename(line)[:6]
        if pt3d_id not in index_map:
            index_map[pt3d_id] = len(pt3d_id_table)
            pt3d_id_table.append([])
        pt3d_id_table[index_map[pt3d_id]].append(count)

    return image_table, pt3d_id_table, len(lines)

def check_valid_pair_list(image_list_name, pair_list_name):
    print "check_valid_pair_list() ..."
    temp = scipy.io.loadmat(pair_list_name)
    pairs = temp['pairs']
    labels = np.squeeze(temp['labels'])
    image_table = load_image_list(image_list_name)
    pair_set = set()

    for i in range(pairs.shape[0]):
        sys.stdout.flush()
        sys.stdout.write("prog: %d/%d             \r" % (i, pairs.shape[0]))
        index1, index2 = pairs[i][0] - 1, pairs[i][1] - 1
        pt3_id1 = os.path.basename(image_table[index1])[:6]
        pt3_id2 = os.path.basename(image_table[index2])[:6]

        if pt3_id1 == pt3_id2:
            assert(labels[i] == 1)
        else:
            assert(labels[i] == -1)

        pairs_tuple = tuple(pairs[i])
        if pairs_tuple not in pair_set:
            pair_set.add(pairs_tuple)
        else:
            print "repeat!"
            #raise AssertionError()

    return 

def calculate_mean_std(input_name, output_name):
    image_table = load_image_list(input_name)
    mean_std = np.zeros((len(image_table), 2), dtype=np.float32)
    sum1, sum2 = 0.0, 0.0
    ## note that the size of all images are the same (128 * 128)
    for index, filename in enumerate(image_table):
        sys.stdout.flush()
        sys.stdout.write("prog: %d/%d          \r" % (index, len(image_table)))
        image = cv2.imread(filename[:-1]).astype(np.float32) / 255.0
        sum1 += np.mean(image)
        sum2 += np.mean(image**2)
    mean = sum1 / float(len(image_table))
    std = np.sqrt((sum2 - 2 * mean * sum1) / float(len(image_table)) + mean**2)
    temp = {}
    temp['mean'] = np.array([mean], dtype=np.float32)
    temp['std'] = np.array([std], dtype=np.float32)
    scipy.io.savemat(output_name, temp)
    return 

if __name__ == "__main__":
    args = parse_args()

    if not os.path.exists(args.list_dir):
        os.mkdir(args.list_dir)

    if args.action == 'genImgList' and args.input_dir == None: 
            print '\033[91m' + 'Action "' + args.action + '" needs input_dir' + '\033[0m'
            exit(-1)

    if (args.action == 'genPairs' or args.action == 'calcMeanStd') and args.input_file == None: 
            print '\033[91m' + 'Action "' + args.action + '" needs input_file' + '\033[0m'
            exit(-1)

    output_name = os.path.join(args.list_dir, args.output)
    if args.action == 'genImgList':
        gen_image_list(args.input_dir, output_name)
    elif args.action == 'genPairs':
        gen_pair_list(args.input_file, output_name)
        check_valid_pair_list(args.input_file, output_name)
    elif args.action == 'calcMeanStd':
        calculate_mean_std(args.input_file, output_name)
    else:
        print '\033[91m' + 'Action "' + args.action + '" is not supported' + '\033[0m'
        exit(-1)

    # gen_image_list('data/Roman_result1/TrainData/pos_patches', os.path.join(list_dir, 'TrainImages.txt'))
    # gen_pair_list(os.path.join(list_dir, 'TrainImages.txt'), os.path.join(list_dir, 'TrainData.mat'))
    # check_valid_pair_list(os.path.join(list_dir, 'TrainImages.txt'), os.path.join(list_dir, 'TrainData.mat'))
    #calculate_mean_std(os.path.join(list_dir, 'TrainImages.txt'), os.path.join(list_dir, 'TrainImages_mean_std.mat'))












