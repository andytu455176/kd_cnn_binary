import numpy as np 
import h5py
import scipy.io

def loadh5(dump_file_full_name):
    ''' Loads a h5 file as dictionary '''

    with h5py.File(dump_file_full_name, 'r') as h5file:
        dict_from_file = readh5(h5file)

    return dict_from_file

def readh5(h5node):
    ''' Recursive function to read h5 nodes as dictionary '''

    dict_from_file = {}
    for _key in h5node.keys():
        if isinstance(h5node[_key], h5py._hl.group.Group):
            dict_from_file[_key] = readh5(h5node[_key])
        else:
            dict_from_file[_key] = h5node[_key].value

    return dict_from_file

def extract_desc_weight(model_name):
    model = loadh5(model_name)
    for k, v in model.iteritems():
        print k 
    output = {}
    output['lift_desc-conv1.W'] = model['l-1-weights']
    output['lift_desc-conv1.b'] = model['l-1-bias']
    output['lift_desc-conv2.W'] = model['l-5-weights']
    output['lift_desc-conv2.b'] = model['l-5-bias']
    output['lift_desc-conv3.W'] = model['l-9-weights']
    output['lift_desc-conv3.b'] = model['l-9-bias']

    output['lift_desc-subt_norm1'] = model['l-4-filter']
    output['lift_desc-subt_norm2'] = model['l-8-filter']

    output['lift_desc-patch_mean'] = model['patch-mean']
    output['lift_desc-patch_std'] = model['patch-std']

    scipy.io.savemat('../lua-code/LIFT_DESC_weight.mat', output)
    return 

def extract_mean_std(model_name):
    model = loadh5(model_name)
    for k, v in model.iteritems():
        print k 
    output = {}
    output['mean'] = model['mean_x']
    output['std'] = model['std_x']
    scipy.io.savemat('../lua-code/LIFT_mean_std.mat', output)
    return 

if __name__ == "__main__":
    # extract_desc_weight('/4t/andytu28/andytu28_history/gihub_repos/LIFT/models/base/new-CNN3-picc-iter-56k.h5')
    extract_mean_std('/4t/andytu28/andytu28_history/gihub_repos/LIFT/models/picc-best/mean_std.h5')









