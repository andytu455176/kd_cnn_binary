A README.md file for python-code. 

# Description of the codes 

* extract_patches.py 

	Just type `python extract_patches.py` for argument information. 
	
	Note that the `--output` argument will construct a new directory with the following structure. 
	
```
OUTPUT 
├── info 
│	├── camera2pt.pkl 
├── pos_patches 
│	├── image1_name 
│	│	├── pos_patch1_on_img1.png
│	│	├── pos_patch2_on_img1.png 
│	│	├── ...
│	├── image2_name 
│	│	├── pos_patch1_on_img2.png
│	│	├── ... 
│	├── ......
├── neg_patches 
``` 

	
* preprocess.py 

	Just type `python preprocess.py` for argument information. 

	Important NOTE: the output name described by `--output` will save the output file to the directory described by `--list_dir`, 
	so there is no need to pass the path using `--output` argument, but it is necessary to pass the path of the input file using `--input_file` argument. 
	
	See `script/make_pairs.sh` and `script/calc_mean_std.sh` for examples. 


* separate_dataset.py 
	
	Only one argument which indicates the directory of the dataset. 

	After executing this program, there will be two new directories, `TestData` and `TrainData`, 
	constructed in `OUTPUT` directory in the above structure. 
	
	The image directories will be moved into the corresponding train or test directory. 

