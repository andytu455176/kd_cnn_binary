#!/bin/bash 

BASE_PATH="$(pwd)"
PYTHON_CODE_PATH="${BASE_PATH}/python-code"

BYellow='\033[1;33m'
END='\033[0m'

echo "${BYellow}==> Generate image list for Romain_dataset Train Data ${END}"
(cd $PYTHON_CODE_PATH; 
 python preprocess.py --action genImgList \
	 				  --list_dir ../dataset_list \
	 				  --input_dir ../data/Roman_dataset/TrainData/pos_patches \
					  --output TrainImages.txt 
)

echo "${BYellow}==> Generate patch pairs for Romain_dataset Train Data ${END}"
(cd $PYTHON_CODE_PATH; 
 python preprocess.py --action genPairs \
	 			   	  --list_dir ../dataset_list \
				   	  --input_file ../dataset_list/TrainImages.txt \
				   	  --output TrainData.mat 
)

echo "${BYellow}==> Generate image list for Romain_dataset Test Data ${END}"
(cd $PYTHON_CODE_PATH; 
 python preprocess.py --action genImgList \
	 				  --list_dir ../dataset_list \
	 				  --input_dir ../data/Roman_dataset/TestData/pos_patches \
					  --output TestImages.txt 
)

echo "${BYellow}==> Generate patch pairs for Romain_dataset Test Data ${END}"
(cd $PYTHON_CODE_PATH; 
 python preprocess.py --action genPairs \
	 			   	  --list_dir ../dataset_list \
				   	  --input_file ../dataset_list/TestImages.txt \
				   	  --output TestData.mat 
)



