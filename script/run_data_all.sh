#!/bin/bash 

echo "run extract_patches.sh to extract keypoint patches ... "
sh script/extract_patches.sh 

echo "run separate_dataset.sh to separate train and test sets ... "
sh script/separate_dataset.sh 

echo "run preprocess.sh to random sample pairs for keypoint patches ... "
sh script/make_pairs.sh 

echo "run calc_mean_std.sh to calculate mean and std for training patches ... "
sh script/calc_mean_std.sh 

echo "run extract_KpPosOri.sh to extract pos and ori for keypoints ... "
sh extract_KpPosOri.sh 

