#!/bin/bash 

BASE_PATH="$(pwd)"
PYTHON_CODE_PATH="${BASE_PATH}/python-code"

BYellow='\033[1;33m'
END='\033[0m'

echo "${BYellow}==> Separate train test set for Roman_dataset ${END}"
(cd $PYTHON_CODE_PATH; \
 python separate_dataset.py ../data/Roman_dataset 
)
