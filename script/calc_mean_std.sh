#!/bin/bash 

BASE_PATH="$(pwd)"
PYTHON_CODE_PATH="${BASE_PATH}/python-code"

BYellow='\033[1;33m'
END='\033[0m'

echo "${BYellow}==> Calculate mean std for Romain_dataset TrainData ${END}"
(cd $PYTHON_CODE_PATH; 
 python preprocess.py --action calcMeanStd \
	 				  --list_dir ../dataset_list \
					  --input_file ../dataset_list/TrainImages.txt \
					  --output TrainImages_mean_std.mat
)


