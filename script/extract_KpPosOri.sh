#!/bin/bash 

BASE_PATH="$(pwd)"
LUA_CODE_PATH="${BASE_PATH}/lua-code"

BYellow='\033[1;33m'
END='\033[0m'

echo "${BYellow}==> Extract pos and ori of keypoints for Train Data of Roman_dataset using LIFT ${END}"
(cd $LUA_CODE_PATH; 
 th run_kpPosOri.lua --image_list_name ../dataset_list/TrainImages.txt \
	 				 --stat_name ../dataset_list/TrainImages_mean_std.mat \
					 --output_name ../dataset_list/TrainImages_KpPosOri.mat \
					 --batch_size 32
)

echo "${BYellow}==> Extract pos and ori of keypoints for Test Data of Roman_dataset using LIFT ${END}"
(cd $LUA_CODE_PATH; 
 th run_kpPosOri.lua --image_list_name ../dataset_list/TestImages.txt \
	 				 --stat_name ../dataset_list/TrainImages_mean_std.mat \
					 --output_name ../dataset_list/TestImages_KpPosOri.mat \
					 --batch_size 32
)

